import React from 'react'
import {Navbar,Nav} from 'react-bootstrap'

const Header =()=>{
    return (
        <div>
            <Navbar bg="light" variant="light">
            <Navbar.Brand href="/">Navbar</Navbar.Brand>
            <Nav className="mr-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/products">Products</Nav.Link>
            <Nav.Link href="/about">About Us</Nav.Link>
            </Nav>
            </Navbar> 
        </div>
    )
}

export default Header