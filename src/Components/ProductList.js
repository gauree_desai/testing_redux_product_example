import React ,{Component} from 'react'
import ProductItem from './ProductItem'
//import json from '../JSON/MusicProducts.json'
import {connect} from 'react-redux'
import fetch_products from '../Actions/ProductActions'


class ProductList extends Component{

//    constructor (){
//        super()
//        this.state={
//            products:json.products //added a state which will be used in render method to iterate over
//        }
//    }
   componentDidMount(){
       this.props.fetchProducts()
   }
    render(){
        //using map method to iterate over array products
        const list= this.props.products?.map((item)=><ProductItem key={item.id} entity={item}></ProductItem>)
        return (
            <div>
                {list}
            </div>
        )
    }
}

const mapStateToProps=(state)=>{
    console.log('mapStateToProps',state)
      return {
          products:state.preducer
      }
  }
  
  const mapDispatchToProps=(dispatch)=>{
      return {
          fetchProducts:()=>dispatch(fetch_products())
      }
  }
  
  //export default ProductList
  export default connect(mapStateToProps,mapDispatchToProps)(ProductList)