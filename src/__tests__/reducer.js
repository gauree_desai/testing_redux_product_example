import ProductReducer from '../Reducers/ProductReducer'

const action={
    type:"FETCH_PRODUCTS",
    payload:[]
}

describe("test reducers",()=>{
    test("handle blank action obj",()=>{
      expect(ProductReducer(undefined,{})).toBe(null)
    })
    test("handle action obj",()=>{
        expect(ProductReducer(undefined,action)).toStrictEqual([])
      })
      
})