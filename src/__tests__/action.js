import configureStore from 'redux-mock-store';
import fetch_products from '../Actions/ProductActions'
import json from '../JSON/MusicProducts.json'

const mockStore = configureStore();
const store = mockStore();

describe("action testing",()=>{

   test("fetch product action payload",()=>{
       store.clearActions()
       store.dispatch(fetch_products())
       const actions = store.getActions()
       const expectedPayload = { type: 'FETCH_PRODUCTS' ,payload:json.products}
       expect(actions).toEqual([expectedPayload])
   })
   
})