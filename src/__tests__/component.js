import renderer from 'react-test-renderer'
import ProductItem from '../Components/ProductItem'
import {BrowserRouter as Router} from 'react-router-dom'
import Header from '../Components/Header'
const entity={
    "id": 1,
    "name": "Grand Piano",
    "price": 44500,
    "type": "manual",
    "description": "Grand piano is great for beginner or adult. It adopted the famous French dream series, its sound source has stable, clearer sound and more vivid expression of the performance of the player.What’s more, keyboard material upgrade, and this piano features multi functions, so you can learn piano more easily and happily!",
    "img": "https://i.ibb.co/wc6qzwW/piano.png"
  }

describe('Component Snapshot testing',()=>{
    test("initial snapshot",()=>{
      const tree=  renderer.create(<ProductItem/>).toJSON()
      expect(tree).toMatchSnapshot()
    })
    test("with param snapshot",()=>{
        const tree=  renderer.create(<Router><ProductItem entity={entity}/></Router>).toJSON()
        expect(tree).toMatchSnapshot()   
    })
    test("Header comp",()=>{
        const tree=  renderer.create(<Header/>).toJSON()
        expect(tree).toMatchSnapshot()  
    })
})