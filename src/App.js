import React from 'react'
import Footer from './Components/Footer'
import Header from './Components/Header'
import ProductList from './Components/ProductList'
import Home from './Components/Home'
import AboutUs from './Components/AboutUs'
import Notfound from './Components/NotFound'
import ProductDetails from './Components/ProductDetails'

import {Route, Switch} from 'react-router-dom'
import { useLocation } from 'react-router-dom'; //this will let us know what route has hit the browser

const path=['/','/products','/about'] //list of valid path
const regPath=/product/;
const App =()=>{
    
    const currentPath=useLocation().pathname

    return (//wrap all the componnets in a Router.

        <div>
           {/* below code will show Header only to valid routes */} 
           {path.includes(currentPath) || regPath.test(currentPath) ?<Header/>:null} 
           <Switch>
              <Route exact path='/' component={Home}></Route>
              <Route path='/about' component={AboutUs}></Route>
              <Route path='/products' component={ProductList}></Route>
              <Route path='/product/:id' component={ProductDetails}></Route>
              <Route component={Notfound}></Route>
           </Switch>    
           {path.includes(useLocation().pathname) ||regPath.test(currentPath) ?<Footer/>:null}
        </div>
      
        
    )
}

export default App