import {createStore, applyMiddleware } from 'redux'
import reducers from '../Reducers'
import { composeWithDevTools } from 'redux-devtools-extension'
let store = createStore(reducers,composeWithDevTools(applyMiddleware()))

export default store