import json from '../JSON/MusicProducts.json'

 function fetch_products(){
    
    return {
        type:'FETCH_PRODUCTS',
        payload:json.products
    }
}
export default fetch_products;